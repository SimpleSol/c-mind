package com.example.tikhon.c_mind.main;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tikhon.c_mind.R;
import com.example.tikhon.c_mind.base.BaseActivity;
import com.example.tikhon.c_mind.task_list.TaskListActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.spOperandCount) Spinner spOperandCount;
    @BindView(R.id.spMaxNumberLength) Spinner spMaxNumberLength;
    @BindView(R.id.cbPlus) CheckBox cbPlus;
    @BindView(R.id.cbMinus) CheckBox cbMinus;
    @BindView(R.id.cbMultiply) CheckBox cbMultiply;
    @BindView(R.id.etTaskCount) EditText etTaskCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.operand_count_entries, R.layout.spinner_item);
        spOperandCount.setAdapter(adapter);
        setupSpinners();
    }

    private void setupSpinners() {
        ArrayAdapter oqAdapter = ArrayAdapter.createFromResource(this, R.array.operand_count_entries,
                R.layout.spinner_item);
        spOperandCount.setAdapter(oqAdapter);
        ArrayAdapter mnlAdapter = ArrayAdapter.createFromResource(this, R.array.max_number_length_entries,
                R.layout.spinner_item);
        spMaxNumberLength.setAdapter(mnlAdapter);
    }

    @OnClick(R.id.btnStart)
    void onClickStart() {
        if (dataIsValid()) {
            startActivity(new Intent(this, TaskListActivity.class));
        }
    }

    private boolean dataIsValid() {
        // checkboxes validation
        if (!cbPlus.isChecked() &&
                !cbMinus.isChecked() &&
                !cbMultiply.isChecked()) {
            Toast.makeText(this, R.string.error_chose_operation, Toast.LENGTH_SHORT).show();
            return false;
        }
        // task count validation
        try {
            int count = Integer.valueOf(etTaskCount.getText().toString());
            if (count <= 0 || count > 100) {
                Toast.makeText(this, R.string.error_task_count, Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.error_task_count, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
