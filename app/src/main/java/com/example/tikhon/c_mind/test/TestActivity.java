package com.example.tikhon.c_mind.test;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tikhon.c_mind.R;
import com.example.tikhon.c_mind.base.BaseActivity;

import net.objecthunter.exp4j.ExpressionBuilder;

import butterknife.BindView;
import butterknife.OnClick;

public class TestActivity extends BaseActivity {

    @BindView(R.id.tvResult) TextView tvResult;
    @BindView(R.id.etExp) EditText etExp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    @OnClick(R.id.btnGo)
    void onClickGo() {
        tvResult.setText(String.valueOf(
                (int) new ExpressionBuilder(etExp.getText().toString())
                        .build().
                        evaluate()
        ));
    }
}
